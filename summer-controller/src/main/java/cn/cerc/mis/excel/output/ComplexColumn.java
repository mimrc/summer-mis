package cn.cerc.mis.excel.output;

import java.util.ArrayList;
import java.util.List;

import cn.cerc.db.core.DataRow;

public class ComplexColumn extends Column {
    private List<String> fields = new ArrayList<>();
    private String delimiter = ",";

    public ComplexColumn() {
        super();
    }

    public ComplexColumn(String[] code, String name, int width) {
        super();
        StringBuffer strBuff = new StringBuffer();
        for (String field : code) {
            fields.add(field);
            strBuff.append(field);
        }
        this.setCode(strBuff.toString());
        this.setName(name);
        this.setWidth(width);
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public String getValue() {
        DataRow record = this.getRecord();
        StringBuffer buff = new StringBuffer();
        for (String field : fields) {
            if (record.hasValue(field)) {
                if (buff.length() > 0) {
                    buff.append(delimiter);
                }
                buff.append(record.getString(field));
            }
        }
        return buff.toString();
    }
}
