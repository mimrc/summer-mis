package cn.cerc.mis.queue;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.mis.client.ServiceSign;
import cn.cerc.mis.message.MessageProcess;

public class AsyncServiceData {
    private String service;

    // 状态列表
    private static final List<String> processTiles = new ArrayList<>();

    static {
        processTiles.add(Lang.get(AsyncServiceData.class, 1, "中止执行"));
        processTiles.add(Lang.get(AsyncServiceData.class, 2, "排队中"));
        processTiles.add(Lang.get(AsyncServiceData.class, 3, "正在执行中"));
        processTiles.add(Lang.get(AsyncServiceData.class, 4, "执行成功"));
        processTiles.add(Lang.get(AsyncServiceData.class, 5, "执行失败"));
        processTiles.add(Lang.get(AsyncServiceData.class, 6, "下载完成"));
    }

    private String corpNo;
    private String userCode;
    private String token;

    // 预约时间，若为空则表示立即执行
    private String timer;
    // 执行进度
    private MessageProcess process = MessageProcess.wait;
    // 处理时间
    private String processTime;
    //
    private DataSet dataIn = new DataSet();
    private DataSet dataOut = new DataSet();

    private IHandle handle;

    public AsyncServiceData(IHandle handle) {
        super();
        this.handle = handle;
        if (handle != null) {
            this.setCorpNo(handle.getCorpNo());
            this.setUserCode(handle.getUserCode());
        }
    }

    public AsyncServiceData(IHandle handle, ServiceSign service) {
        this(handle);
        this.setService(service);
    }

    public AsyncServiceData(IHandle handle, String service) {
        this(handle);
        this.setService(service);
    }

    public static String getProcessTitle(int process) {
        return processTiles.get(process);
    }

    public AsyncServiceData read(String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readTree(jsonString);
        this.setService(json.get("service").asText());
        if (json.has("dataOut"))
            this.dataOut().setJson(json.get("dataOut").asText());

        if (json.has("dataIn"))
            this.dataIn().setJson(json.get("dataIn").asText());

        if (json.has("process"))
            this.setProcess(MessageProcess.values()[json.get("process").asInt()]);

        if (json.has("timer"))
            this.setTimer(json.get("timer").asText());

        if (json.has("processTime"))
            this.setProcessTime(json.get("processTime").asText());

        if (json.has("token"))
            this.setToken(json.get("token").asText());
        return this;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    private String toJson() {
        ObjectNode content = new ObjectMapper().createObjectNode();
        content.put("service", this.service);
        if (this.dataIn() != null) {
            content.put("dataIn", dataIn().json());
        }
        if (this.dataOut() != null) {
            content.put("dataOut", dataOut().json());
        }
        content.put("timer", this.timer);
        content.put("token", this.token);
        content.put("process", this.process.ordinal());
        if (this.processTime != null) {
            content.put("processTime", this.processTime);
        }
        return content.toString();
    }

    public DataSet dataIn() {
        return dataIn;
    }

    public String getService() {
        return service;
    }

    public AsyncServiceData setService(String service) {
        this.service = service;
        return this;
    }

    public AsyncServiceData setService(ServiceSign service) {
        this.service = service.id();
        return this;
    }

    public MessageProcess getProcess() {
        return process;
    }

    public void setProcess(MessageProcess process) {
        this.process = process;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getProcessTime() {
        return processTime;
    }

    public void setProcessTime(String processTime) {
        this.processTime = processTime;
    }

    public String getCorpNo() {
        return corpNo;
    }

    public void setCorpNo(String corpNo) {
        this.corpNo = corpNo;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSubject() {
        return dataIn().head().getString("_subject_");
    }

    public void setSubject(String subject) {
        dataIn().head().setValue("_subject_", subject);
    }

    public void setSubject(String format, Object... args) {
        dataIn().head().setValue("_subject_", String.format(format, args));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DataSet dataOut() {
        return dataOut;
    }

    public IHandle getHandle() {
        return handle;
    }

}
