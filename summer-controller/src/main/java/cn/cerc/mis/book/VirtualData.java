package cn.cerc.mis.book;

import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.Datetime.DateType;

public class VirtualData implements IBookData {
    private Datetime date;
    private UpdateBook book;
    private IBookData bookData;

    public VirtualData(UpdateBook book, IBookData bookData, int month) {
        this.book = book;
        this.bookData = bookData;
        this.date = bookData.getDate().inc(DateType.Month, month).toMonthBof();
    }

    public IBookData getBookData() {
        return bookData;
    }

    public UpdateBook getBook() {
        return book;
    }

    @Override
    public Datetime getDate() {
        return date;
    }

    @Override
    public boolean check() {
        return true;
    }

}
