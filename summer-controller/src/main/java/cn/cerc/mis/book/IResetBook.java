package cn.cerc.mis.book;

import cn.cerc.db.core.DataException;

/**
 * 对登记到帐本的的数据进行重置（回算）
 */
public interface IResetBook extends IBookEnroll {
    // 初始化（仅调用一次）
    void init(BatchManager manage);

    // 初始化（ 会被每月调用）
    void ready() throws DataException;

    // 将帐本的更新，保存到数据库中
    void save() throws DataException;

    /**
     * 将帐本的本期数据设置为期初，本期发生额设置为0
     * 
     * @throws DataException
     */
    void reset() throws DataException;

}
