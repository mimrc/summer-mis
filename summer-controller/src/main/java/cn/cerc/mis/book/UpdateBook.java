package cn.cerc.mis.book;

import cn.cerc.db.core.DataException;

public interface UpdateBook extends IBookEnroll {
    // 初始化（仅调用一次）
    void init(BatchManager manage);

    // 初始化（ 会被每月调用）
    void ready() throws DataException;

    // 将帐本的更新，保存到数据库中
    void save() throws DataException;

    // 在过帐时，需要区分年月
    boolean isKnowMonth();

    // 对登记到帐本的的数据进行更新
    void update() throws DataException;
}
