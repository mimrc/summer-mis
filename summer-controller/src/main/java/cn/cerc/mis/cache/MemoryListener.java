package cn.cerc.mis.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import cn.cerc.db.redis.JedisFactory;
import cn.cerc.mis.core.Application;
import cn.cerc.mis.core.BasicHandle;
import cn.cerc.mis.core.SystemBuffer;
import cn.cerc.mis.other.MemoryBuffer;
import redis.clients.jedis.Jedis;

/**
 * 内存缓存清理信号发射器
 */
@Component
public class MemoryListener {
    private static final Logger log = LoggerFactory.getLogger(MemoryListener.class);
    public static final String CacheChannel = MemoryBuffer.buildKey(SystemBuffer.Global.CacheReset);

    public static void refresh(Class<? extends IMemoryCache> clazz, String param) {
        IMemoryCache cache = Application.getBean(clazz);
        if (cache == null)
            return;
        final String beanId = cache.getBeanName();
        try (Jedis jedis = JedisFactory.getJedis()) {
            if (jedis != null)
                jedis.publish(CacheChannel, param == null ? beanId : (beanId + ":" + param));
        }
    }

    public static void resetCache(ApplicationContext context, CacheResetMode resetType) {
        // 通知所有的单例重启缓存
        Application.setContext(context);
        try (BasicHandle handle = new BasicHandle()) {
            for (String beanId : context.getBeanDefinitionNames()) {
                if (context.isSingleton(beanId)) {
                    Object bean = context.getBean(beanId);
                    if (bean instanceof IMemoryCache cache) {
                        cache.resetCache(handle, resetType, null);
                        log.debug("{}.resetCache", beanId);
                    }
                }
            }
        }
    }

}
