package cn.cerc.mis.config;

import org.springframework.stereotype.Component;

import cn.cerc.db.core.ConfigReader;
import cn.cerc.db.core.IAppConfig;

@Component
public class AppConfigDefault implements IAppConfig {

    /**
     * @return 返回默认的欢迎页
     */
    @Override
    public String getWelcomePage() {
        ConfigReader config = ConfigReader.instance();
        return config.getProperty(IAppConfig.FORM_WELCOME, "welcome");
    }

    /**
     * @return 返回默认的主菜单
     */
    @Override
    public String getDefaultPage() {
        ConfigReader config = ConfigReader.instance();
        return config.getProperty(IAppConfig.FORM_DEFAULT, "FrmDefault");
    }

    /**
     * @return 退出系统确认画面
     */
    @Override
    public String getLogoutPage() {
        ConfigReader config = ConfigReader.instance();
        return config.getProperty(IAppConfig.FORM_LOGOUT, "logout");
    }

}
