package cn.cerc.mis.log;

import java.util.List;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.classic.spi.ThrowableProxyUtil;
import ch.qos.logback.core.AppenderBase;
import cn.cerc.db.core.ConfigReader;
import cn.cerc.db.exception.IKnowall;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.log.KnowallLog;
import cn.cerc.db.tool.LogUtils;

public class JayunLogbackAppender extends AppenderBase<ILoggingEvent> {
    /** 采样分级 */
    private final List<Level> levels = List.of(Level.ERROR, Level.WARN, Level.INFO);
    /** 是否上报 */
    private Boolean upload = null;

    @Override
    protected void append(ILoggingEvent event) {
        // 灰度发布不发送日志到测试平台
        if (upload == null)
            upload = !ConfigReader.instance().isServerGray();
        if (!upload.booleanValue())
            return;

        // 将日志事件交给日志解析器处理
        Level level = event.getLevel();
        if (levels.stream().noneMatch(item -> level == item))
            return;

        // 调用位置
        StackTraceElement callerData = event.getCallerData()[0];
        String origin = String.join(":", callerData.getClassName(), callerData.getLineNumber() + "");
        if (origin.startsWith("com.mongodb.internal.diagnostics.logging"))
            return;
        if (origin.startsWith("org.springframework"))
            return;
        if (origin.startsWith("org.apache.juli.logging.DirectJDKLog"))
            return;

        IThrowableProxy proxy = event.getThrowableProxy();
        KnowallLog logger = new KnowallLog(origin);
        logger.setMessage(event.getFormattedMessage());
        logger.setLevel(level.toString().toLowerCase());
        if (proxy == null) { // 无异常对象的，直接提交并返回
            logger.post();
            return;
        }

        // 取得异常对象
        if (!(proxy instanceof ThrowableProxy))
            return;
        try {
            ThrowableProxy proxy1 = (ThrowableProxy) proxy;
            Throwable cause1 = proxy1.getThrowable();
            while (cause1 != null) {
                if (cause1 instanceof KnowallException data) {
                    LogUtils.error(data.getClass().getName());
                    logger.setType(data.getClass().getSimpleName());
                    logger.add(data.getItems());
                } else if (cause1 instanceof IKnowall data) {
                    LogUtils.error(data.getClass().getName());
                    logger.add(data.getData());
                } else {
                    logger.setType(cause1.getClass().getSimpleName());
                }
                cause1 = cause1.getCause();
            }
            // 获取堆栈信息
            IThrowableProxy cause2 = proxy.getCause();
            while (cause2 != null) {
                logger.add(ThrowableProxyUtil.asString(proxy));
                cause2 = cause2.getCause();
            }
            logger.post();
        } catch (Exception e) {
            LogUtils.error(e.getMessage());
            e.printStackTrace();
        }
    }

}
