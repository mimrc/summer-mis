package cn.cerc.mis.security;

import cn.cerc.db.core.ISession;

public interface AutoLogin {

    /**
     * 在成功登录后,记得给 sesion中的token等赋值!
     * 
     * @param session 当前 session
     */
    void execute(ISession session);

}
