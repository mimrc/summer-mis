package cn.cerc.mis.client;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataSet;
import cn.cerc.db.tool.JsonTool;

public class WebcallDecoderDefault implements WebcallDecoder {
    private static final Logger log = LoggerFactory.getLogger(WebcallDecoderDefault.class);

    @Override
    public Object get(Class<?> returnType, String response) {
        if (returnType == DataSet.class) {
            return new DataSet().setJson(response);
        } else {
            var json = new JSONObject(response);
            if (json.has("result")) {
                if (json.getBoolean("result")) {
                    if (json.has("data")) {
                        return JsonTool.fromJson(json.get("data").toString(), returnType);
                    } else
                        return null;
                } else {
                    if (json.has("message"))
                        log.error(json.getString("message"));
                    return null;
                }
            } else {
                return JsonTool.fromJson(response, returnType);
            }
        }
    }

}
