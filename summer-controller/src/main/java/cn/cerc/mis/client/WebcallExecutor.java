package cn.cerc.mis.client;

import java.lang.reflect.Method;

public interface WebcallExecutor {

    Object get(Webcall webcall, String url, Method method, Object[] args);

    default void encoder(WebcallEncoder encoder) {

    }

    default void decoder(WebcallDecoder decoder) {

    }

    /**
     * 
     * @return 调用超时设置，单位：毫秒
     */
    default int getReadTimeout() {
        return 10000;
    }

    /**
     * 
     * @param readTimeout 设置调用超时时间, 默认 10000 毫秒
     */
    default void setReadTimeout(int readTimeout) {
    }

}
