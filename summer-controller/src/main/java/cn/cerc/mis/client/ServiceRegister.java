package cn.cerc.mis.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.cerc.db.queue.ServerManager;

@Component
@Deprecated
public class ServiceRegister {
    @Autowired
    private ServerManager serverManager;

    public static String extranet;

    /**
     * @return 返回可用的服务地址
     */
    public ServiceSiteRecord getServiceHost(String industry) {
        var host = "http://" + serverManager.getHost(industry, true);
        return new ServiceSiteRecord(true, industry, host);
    }

}
