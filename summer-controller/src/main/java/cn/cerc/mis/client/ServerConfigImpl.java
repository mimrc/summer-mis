package cn.cerc.mis.client;

import java.util.Optional;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.IHandle;

public interface ServerConfigImpl {

    /**
     * 获取指定帐套的产业别
     * 
     * @param handle 句柄
     * @param corpNo 目标的帐套代码
     */
    Optional<String> getIndustry(IHandle handle, String corpNo) throws DataException;

    /**
     * 获取指定帐套的授权代码
     * 
     * @param handle 句柄
     * @param corpNo 目标的帐套代码
     * @return 返回目标帐套授权的 token
     */
    Optional<String> getToken(IHandle handle, String corpNo) throws DataException;

    /**
     * 获取指定帐套的服务节点
     * 
     * @param handle 句柄
     * @param corpNo 目标的帐套代码
     * @return
     */
    ServerSupplier getServer(IHandle handle, String corpNo);
}
