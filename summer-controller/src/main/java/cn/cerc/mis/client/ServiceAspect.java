package cn.cerc.mis.client;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.SpringBean;
import cn.cerc.mis.core.DataValidate;
import cn.cerc.mis.core.DataValidateException;
import cn.cerc.mis.core.IService;
import cn.cerc.mis.core.PerformanceManager;
import cn.cerc.mis.security.SecurityPolice;
import cn.cerc.mis.security.SecurityStopException;

@Aspect
@Component
public class ServiceAspect {
    private static final Logger log = LoggerFactory.getLogger(ServiceAspect.class);

    @Autowired
    private SecurityPolice securityPolice;

    @Pointcut("@annotation(cn.cerc.mis.core.DataValidate)")
    public void dataValidate() {

    }

    @Pointcut("@annotation(cn.cerc.mis.core.DataValidates)")
    public void dataValidates() {

    }

    @Pointcut("@annotation(cn.cerc.mis.security.Permission)")
    public void permission() {

    }

    @Around("dataValidate() || dataValidates() || permission()")
    public Object validateData(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        Class<?> returnType = method.getReturnType();
        boolean returnDataSet = returnType.isAssignableFrom(DataSet.class);
        if (!returnDataSet) {
            log.warn("服务 {} 返回值请改为 DataSet 类型", joinPoint.getSignature());
        }

        Object[] args = joinPoint.getArgs();
        if (args.length == 0 || args.length > 2) {
            log.warn("方法 {} 校验时参数数量不符合规范，请改正", joinPoint.getSignature());
        }

        // 取关键参数
        IHandle handle = null;
        DataRow headIn = null;
        DataSet dataIn = null;
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (i == 0 && args[i] instanceof IHandle) {
                handle = (IHandle) arg;
            } else if (i == 1) {
                if (arg instanceof DataRow row) {
                    headIn = row;
                    dataIn = row.toDataSet();
                } else if (arg instanceof DataSet dataSet) {
                    headIn = dataSet.head();
                    dataIn = dataSet;
                }
            }
        }

        if (headIn == null && IService.class.isAssignableFrom(joinPoint.getTarget().getClass())) {
            dataIn = RemoteProxy.buildDataIn(method, args);
            headIn = dataIn.head();
        }
        // 参数校验
        if (headIn != null) {
            DataValidate[] validates = method.getAnnotationsByType(DataValidate.class);
            for (DataValidate validate : validates) {
                String fieldCode = validate.value();
                if (!headIn.hasValue(fieldCode)) {
                    String errorMsg = validate.message();
                    if (validate.message().contains("%s")) {
                        String fieldName = "".equals(validate.name()) ? fieldCode : validate.name();
                        errorMsg = String.format(validate.message(), fieldName);
                    }
                    if (returnDataSet) {
                        return new DataSet().setMessage(errorMsg);
                    } else {
                        throw new DataValidateException(errorMsg);
                    }
                }
            }
        }

        // 权限校验
        if (handle != null) {
            try {
                securityPolice.check(handle, method, joinPoint.getTarget());
            } catch (SecurityStopException e) {
                if (returnDataSet) {
                    return new DataSet().setMessage(e.getMessage());
                } else {
                    throw e;
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("{}.{}, dataIn: {}", joinPoint.getTarget().getClass().getName(),
                    joinPoint.getSignature().getName(), dataIn);
//            SpringBean.printStackTrace(joinPoint.getTarget().getClass().getName(), 0);
        }
        var start = System.currentTimeMillis();
        var result = joinPoint.proceed();
        if ((System.currentTimeMillis() - start) > 1000) {
            if (handle != null) {
                String corpNo = handle.getCorpNo();
                String userCode = handle.getUserCode();
                var context = SpringBean.context();
                if (context.getBeanNamesForType(PerformanceManager.class).length == 1) {
                    var bean = context.getBean(PerformanceManager.class);
                    bean.writeServiceExecuteTime(corpNo, userCode, dataIn, joinPoint.getSignature().getName(), start);
                }
            }
            log.debug("timer: {}.{}", joinPoint.getTarget().getClass().getName(), joinPoint.getSignature().getName());
        }
        return result;
    }

    @AfterReturning(value = "target(cn.cerc.mis.core.IService)", returning = "result")
    public void dataOut(JoinPoint joinPoint, Object result) {
        if (result instanceof DataSet dataOut) {
//            if (log.isDebugEnabled()) {
//                log.debug("AfterReturning: {}.{}", joinPoint.getTarget().getClass().getName(),
//                        joinPoint.getSignature().getName());
//            }
            if (joinPoint.getArgs() != null && joinPoint.getArgs().length > 0)
                dataOut.disableStorage().first();
        }
    }

}
