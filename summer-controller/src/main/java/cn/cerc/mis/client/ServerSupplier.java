package cn.cerc.mis.client;

import java.util.List;

import cn.cerc.db.core.Utils;

public interface ServerSupplier {

    default String getHost() {
        List<String> hosts = this.getHosts();
        if (hosts == null || hosts.size() == 0)
            throw new RuntimeException("hosts is empty");
        else if (hosts.size() == 1)
            return hosts.get(0);
        else {
            int index = Utils.random(0, hosts.size() - 1);
            return hosts.get(index);
        }
    }

    List<String> getHosts();

}
