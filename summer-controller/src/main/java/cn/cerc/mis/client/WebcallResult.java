package cn.cerc.mis.client;

public class WebcallResult {
    private boolean result;
    private String message;
    private Object data;

    public WebcallResult(boolean result, String message) {
        super();
        this.result = result;
        this.message = message;
    }

    public WebcallResult(boolean result, String message, Object data) {
        super();
        this.result = result;
        this.message = message;
        this.data = data;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
