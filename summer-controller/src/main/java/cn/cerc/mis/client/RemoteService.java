package cn.cerc.mis.client;

import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import cn.cerc.db.core.ClassResource;
import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;
import cn.cerc.mis.SummerMIS;
import cn.cerc.mis.core.LocalService;

public class RemoteService extends ServiceProxy {
    private static final Logger log = LoggerFactory.getLogger(RemoteService.class);
    private static final ClassResource res = new ClassResource(RemoteService.class, SummerMIS.ID);
    private ServiceSign sign;

    public RemoteService(IHandle handle) {
        super();
        this.setSession(handle.getSession());
    }

    public ServiceSign sign() {
        return this.sign;
    }

    public void setSign(ServiceSign sign) {
        this.sign = sign;
    }

    public boolean exec(Object... args) {
        if (args.length > 0) {
            DataRow headIn = dataIn().head();
            if (args.length % 2 != 0)
                throw new RuntimeException(res.getString(1, Lang.get(RemoteService.class, 1, "传入的参数数量必须为偶数！")));
            for (int i = 0; i < args.length; i = i + 2)
                headIn.setValue(args[i].toString(), args[i + 1]);
        }
        this.setDataOut(this.sign.callLocal(this, this.dataIn()).dataOut());
        return this.isOk();
    }

    /**
     * 根据帐套配置，调用相应的机群服务
     */
    public static DataSet call(IHandle handle, CorpConfigImpl targetConfig, String key, DataSet dataIn,
            ServerOptionImpl serviceOption) throws DataException {
        Objects.requireNonNull(targetConfig);
        // 防止本地调用
        if (targetConfig.isLocal()) {
            if (!"000000".equals(targetConfig.getCorpNo())) {
                String message = String.format(
                        Lang.get(RemoteService.class, 2, "%s, %s 发起帐套和目标帐套相同，应改使用 callLocal 来调用，dataIn %s"), key,
                        handle.getCorpNo(), dataIn.json());
                RuntimeException e = new RuntimeException(message);
                log.warn("{}", message, e);
            }
            return LocalService.call(key, handle, dataIn);
        } else if (serviceOption != null) {
            // 处理特殊的业务场景，创建帐套、钓友商城
            // 获取指定的目标机节点
            ServerSupplier supplier = serviceOption.getServerSupplier(handle, key);
            // 获取指定的目标机授权
            var token = serviceOption.getToken(handle).orElse(null);
            if (supplier == null || token == null) {
                // 用于自建的私服企业
                var server = RemoteService.getServerConfig(SpringBean.context());
                if (server.isPresent()) {
                    if (supplier == null) {
                        String endpoint = server.get().getServer(handle, targetConfig.getCorpNo()).getHost()
                                + "/services/";
                        if (endpoint != null)
                            supplier = new ServerSimple(endpoint);
                    }
                    if (token == null)
                        token = server.get().getToken(handle, targetConfig.getCorpNo()).orElse(null);
                }
            }
            if (supplier == null)
                throw new RuntimeException(Lang.get(RemoteService.class, 3, "endpoint 不允许为空"));
            return RemoteRequest.request(supplier, token, key, dataIn);
        } else {
            // 若未告诉服务器，则取默认设置
            ServerConfigImpl defaultConfig = RemoteService.getServerConfig(SpringBean.context()).orElseThrow(
                    () -> new RuntimeException(Lang.get(RemoteService.class, 4, "无法获取到有效的微服务配置 ServerConfigImpl")));
            String endpoint = defaultConfig.getServer(handle, targetConfig.getCorpNo()).getHost() + "/services/";
            String token = defaultConfig.getToken(handle, targetConfig.getCorpNo()).orElse(null);
            return RemoteRequest.request(new ServerSimple(endpoint), token, key, dataIn);
        }
    }

    public static Optional<ServerConfigImpl> getServerConfig(ApplicationContext context) {
        if (context != null) {
            try {
                return Optional.of(context.getBean(ServerConfigImpl.class));
            } catch (NoSuchBeanDefinitionException e) {
                log.warn(Lang.get(RemoteService.class, 10, "微服务异常：未找到实现 ServerConfigImpl 的类"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return Optional.empty();
    }

    @Deprecated
    public static DataSet request(String host, String token, String service, DataSet dataIn) {
        return RemoteRequest.request(new ServerSimple(host), token, service, dataIn);
    }
}
