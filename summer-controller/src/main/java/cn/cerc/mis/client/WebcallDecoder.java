package cn.cerc.mis.client;

public interface WebcallDecoder {
    /**
     * 
     * @param returnType
     * @param response
     * @return 将 response 解析为 returnType 对象
     */
    Object get(Class<?> returnType, String response);

}
