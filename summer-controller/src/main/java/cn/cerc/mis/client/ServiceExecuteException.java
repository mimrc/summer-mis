package cn.cerc.mis.client;

import cn.cerc.db.core.DataException;

/**
 * 服务执行异常
 */
public class ServiceExecuteException extends DataException {
    private static final long serialVersionUID = 665054396844872223L;

    public ServiceExecuteException(String message) {
        super(message);
    }
}
