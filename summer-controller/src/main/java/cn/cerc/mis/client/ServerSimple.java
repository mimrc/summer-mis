package cn.cerc.mis.client;

import java.util.List;

public class ServerSimple implements ServerSupplier {
    private String host;

    public ServerSimple(String host) {
        this.host = host;
    }

    @Override
    public List<String> getHosts() {
        return List.of(host);
    }

}
