package cn.cerc.mis.client;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonSyntaxException;

import cn.cerc.db.core.Curl;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.ISession;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.Utils;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.tool.JsonTool;
import cn.cerc.mis.core.ServiceState;

public class RemoteRequest {
    private static final Logger log = LoggerFactory.getLogger(RemoteRequest.class);

    /**
     * 调用远程服务
     *
     * @param supplier 远程服务器 url
     * @param token    远程服务器访问 token
     * @param service  服务签名 SvrUserInfo.search
     * @param dataIn   服务参数
     * @return 服务返回值
     */
    public static DataSet request(ServerSupplier supplier, String token, String service, DataSet dataIn) {
        String endpoint = supplier.getHost();
        Objects.requireNonNull(endpoint);
        Curl curl = new Curl();
        if (!Utils.isEmpty(token))
            curl.put(ISession.TOKEN, token);
        if (Utils.isEmpty(service))
            throw new RuntimeException("service is null");
        curl.put("dataIn", dataIn.json());

        long retryInterval = 1000; // 重试间隔1秒
        long maxRetryDuration = TimeUnit.SECONDS.toMillis(3); // 最大重试时间3秒
        int tryTimes = Utils.ceil((double) maxRetryDuration / retryInterval);

        AtomicInteger atomic = new AtomicInteger();
        DataSet dataSet = new DataSet();
        for (int i = 0; i < tryTimes; i++) {
            atomic.incrementAndGet();
            String response = "";

            long startTime = System.currentTimeMillis();
            try {
                response = curl.doPost(endpoint + service);
                dataSet.setJson(response);
                break;
            } catch (IOException e) {
                if (e instanceof SocketTimeoutException) {
                    log.error("{}{} 远程服务连接超时 {}", endpoint, service, e.getMessage(), new KnowallException
                            (new RemoteServiceTimeoutException(e))
                            .addMapOf(Lang.get(RemoteService.class, 5, "入参信息"), JsonTool.toJson(curl.getParameters()))
                            .addMapOf(Lang.get(RemoteService.class, 6, "返回信息"), response)
                            .addMapOf(Lang.get(RemoteService.class, 7, "耗时"),
                                    String.valueOf(System.currentTimeMillis() - startTime))
                            .addMapOf(Lang.get(RemoteService.class, 8, "累计重试"), String.valueOf(atomic.get())));
                    dataSet.clear();
                    dataSet.setState(ServiceState.CALL_TIMEOUT).setMessage("remote service timeout");
                } else {
                    log.error("{}{} 远程服务访问异常 {}", endpoint, service, e.getMessage(), e);
                    dataSet.clear();
                    dataSet.setState(ServiceState.ERROR).setMessage("remote service error");
                }
            } catch (JsonSyntaxException e) {
                log.error("{}{} 远程服务访问异常 {}", endpoint, service, e.getMessage(),
                        new KnowallException(e)
                                .addMapOf(Lang.get(RemoteService.class, 5, "入参信息"), JsonTool.toJson(curl.getParameters()))
                                .addMapOf(Lang.get(RemoteService.class, 6, "返回信息"), response)
                                .addMapOf(Lang.get(RemoteService.class, 8, "累计重试"), String.valueOf(atomic.get())));
                dataSet.clear();
                dataSet.setState(ServiceState.ERROR).setMessage("remote service error");
            }
            try {
                // 等待重试间隔
                Thread.sleep(retryInterval);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException occurred: " + e.getMessage());
            }
        }
        return dataSet;
    }
}
