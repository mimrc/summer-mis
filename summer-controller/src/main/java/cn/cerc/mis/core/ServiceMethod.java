package cn.cerc.mis.core;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.log.KnowallException;
import cn.cerc.mis.security.SecurityPolice;
import cn.cerc.mis.security.SecurityStopException;

public final class ServiceMethod {
    private static final Logger log = LoggerFactory.getLogger(ServiceMethod.class);
    private final Method method;
    private final ServiceMethodVersion version;
    private static SecurityPolice securityPolice;

    public enum ServiceMethodVersion {
        ResultBoolean, ResultStatus, ResultDataSetByDataIn, ResultDataSetByHeadIn, ResultBooleanByDataIn,
        ResultBooleanByHeadIn, ResultDataSetByEmpty
    }

    public ServiceMethod(Method method, ServiceMethodVersion version) {
        this.method = method;
        this.version = version;
    }

    public Method method() {
        return method;
    }

    public ServiceMethodVersion version() {
        return version;
    }

    public DataSet call(Object owner, IHandle handle, DataSet dataIn) {
        try {
            // 调用数据校验
            DataValidate[] validates = method.getDeclaredAnnotationsByType(DataValidate.class);
            for (DataValidate validate : validates) {
                DataRow headIn = dataIn.head();
                String errorMsg = validate.message();
                String fieldCode = validate.value();
                if (!headIn.hasValue(fieldCode)) {
                    if (errorMsg.contains("%s")) {
                        String message = "".equals(validate.name()) ? fieldCode : validate.name();
                        throw new DataValidateException(String.format(errorMsg, message));
                    } else
                        throw new DataValidateException(errorMsg);
                }
            }
            // 执行权限检查
            if (securityPolice == null)
                securityPolice = SpringBean.get(SecurityPolice.class);
            try {
                securityPolice.check(handle, method, owner);
            } catch (SecurityStopException e) {
                return new DataSet().setMessage(e.getMessage());
            }
            // 执行具体的服务函数
            DataSet dataOut;
            switch (this.version) {
            case ResultBoolean: {
//                log.warn("2025年起不再支持此种写法，请尽快升级: {}.{}", owner.getClass().getSimpleName(), method.getName());
                if (owner instanceof CustomService) {
                    boolean result = (Boolean) method.invoke(owner);
                    dataOut = ((CustomService) owner).dataOut();
                    dataOut.setState(result ? ServiceState.OK : ServiceState.ERROR);
                } else {
                    dataOut = new DataSet().setMessage("It not is CustomService");
                }
                break;
            }
            case ResultStatus: {
//                log.warn("2025年起不再支持此种写法，请尽快升级: {}.{}", owner.getClass().getSimpleName(), method.getName());
                dataOut = new DataSet();
                IStatus result = (IStatus) method.invoke(owner, dataIn, dataOut);
                if (dataOut.state() == ServiceState.ERROR)
                    dataOut.setState(result.getState());
                if (dataOut.message() == null)
                    dataOut.setMessage(result.getMessage());
                break;
            }
            case ResultDataSetByDataIn: {
                dataOut = (DataSet) method.invoke(owner, handle, dataIn);
                break;
            }
            case ResultDataSetByHeadIn: {
                dataOut = (DataSet) method.invoke(owner, handle, dataIn.head());
                break;
            }
            case ResultDataSetByEmpty: {
                dataOut = (DataSet) method.invoke(owner, handle);
                break;
            }
            case ResultBooleanByDataIn: {
//                log.warn("2025年起不再支持此种写法，请尽快升级: {}.{}", owner.getClass().getSimpleName(), method.getName());
                boolean result = (Boolean) method.invoke(owner, handle, dataIn);
                dataOut = new DataSet().setState(result ? ServiceState.OK : ServiceState.ERROR);
                break;
            }
            case ResultBooleanByHeadIn: {
//                log.warn("2025年起不再支持此种写法，请尽快升级: {}.{}", owner.getClass().getSimpleName(), method.getName());
                boolean result = (Boolean) method.invoke(owner, handle, dataIn.head());
                dataOut = new DataSet().setState(result ? ServiceState.OK : ServiceState.ERROR);
                break;
            }
            default: {
//                log.warn("2025年起不再支持此种写法，请尽快升级: {}.{}", owner.getClass().getSimpleName(), method.getName());
                dataOut = new DataSet().setMessage("can't support " + this.version.name());
                break;
            }
            }
            // 防止调用者修改并回写到数据库
            dataOut.disableStorage().first();
            return dataOut;
        } catch (Exception e) {
            Throwable throwable = e.getCause() != null ? e.getCause() : e;
            if (throwable instanceof DataException)
                log.warn("{}.{} -> {}", owner.getClass().getSimpleName(), method.getName(), throwable.getMessage(),
                        new KnowallException(throwable).addMapOf("method", method.getName()).addMapOf("dataIn",
                                dataIn.json()));
            else
                log.warn("{}.{} -> {}", owner.getClass().getSimpleName(), method.getName(), throwable.getMessage(),
                        new KnowallException(throwable).addMapOf("method", method.getName()).addMapOf("dataIn",
                                dataIn.json()));
            return new DataSet().setMessage(throwable.getMessage());
        }
    }

    public static ServiceMethod build(Class<?> clazz, String funcCode) {
        // 第1代版本：不支持单例
        try {
            Method method = clazz.getMethod(funcCode);
            if (method.getModifiers() != Modifier.PUBLIC)
                return null;
            if (method.getReturnType() != boolean.class)
                return null;
            else
                return new ServiceMethod(method, ServiceMethodVersion.ResultBoolean);
        } catch (NoSuchMethodException | SecurityException e1) {
        }
        // 第2代版本：不支持单例
        try {
            Method method = clazz.getMethod(funcCode, DataSet.class, DataSet.class);
            if (method.getModifiers() != Modifier.PUBLIC)
                return null;
            if (method.getReturnType() != IStatus.class)
                return null;
            else
                return new ServiceMethod(method, ServiceMethodVersion.ResultStatus);
        } catch (NoSuchMethodException | SecurityException e1) {

        }
        // 第3代版本：支持单例
        try {
            Method method = clazz.getMethod(funcCode, IHandle.class, DataSet.class);
            if (method.getModifiers() != Modifier.PUBLIC)
                return null;
            if (method.getReturnType() == DataSet.class)
                return new ServiceMethod(method, ServiceMethodVersion.ResultDataSetByDataIn);
            else if (method.getReturnType() == boolean.class)
                return new ServiceMethod(method, ServiceMethodVersion.ResultBooleanByDataIn);
            else
                return null;
        } catch (NoSuchMethodException | SecurityException e1) {
        }
        // 第4代版本：支持单例
        try {
            Method method = clazz.getMethod(funcCode, IHandle.class, DataRow.class);
            if (method.getModifiers() != Modifier.PUBLIC)
                return null;
            if (method.getReturnType() == DataSet.class)
                return new ServiceMethod(method, ServiceMethodVersion.ResultDataSetByHeadIn);
            else if (method.getReturnType() == boolean.class)
                return new ServiceMethod(method, ServiceMethodVersion.ResultBooleanByHeadIn);
            else
                return null;
        } catch (NoSuchMethodException | SecurityException e1) {
        }
        // 第5代版本：支持单例
        try {
            Method method = clazz.getMethod(funcCode, IHandle.class);
            if (method.getModifiers() != Modifier.PUBLIC)
                return null;
            if (method.getReturnType() == DataSet.class)
                return new ServiceMethod(method, ServiceMethodVersion.ResultDataSetByEmpty);
            else
                return null;
        } catch (NoSuchMethodException | SecurityException e1) {
        }
        // 没有找到指定的函数
        return null;
    }

}
