package cn.cerc.mis.core;

public interface RedirectForm {

    IForm getRedirectForm(IForm owner);

}
