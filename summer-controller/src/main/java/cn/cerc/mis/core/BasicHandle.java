package cn.cerc.mis.core;

import org.springframework.beans.factory.annotation.Autowired;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.TaskSession;
import cn.cerc.mis.security.SessionFactory;

public class BasicHandle implements IHandle, AutoCloseable {
    private ISession session;

    public BasicHandle() {
        super();
    }

    public BasicHandle(String token) {
        super();
        var session = SpringBean.get(TaskSession.class);
        SessionFactory.loadToken(session, token);
        this.setSession(session);
    }

    @Override
    public ISession getSession() {
        if (session == null)
            session = SpringBean.get(TaskSession.class);
        return session;
    }

    @Autowired
    @Override
    public void setSession(ISession session) {
        this.session = session;
    }

    @Override
    public void close() {
        if (session != null) {
            session.close();
            session = null;
        }
    }

}
