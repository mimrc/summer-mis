package cn.cerc.mis.core;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.Datetime.DateType;
import cn.cerc.db.core.MD5;
import cn.cerc.mis.other.MemoryBuffer;
import jakarta.annotation.Resource;

/**
 * 对日期范围进行分割
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SegmentQuery {
    private AppClient client;
    private DataSet dataIn;
    private DataSet dataOut;
    private boolean firstTime = true;

    public SegmentQuery init(CustomService owner) {
        this.dataIn = owner.dataIn();
        this.dataOut = owner.dataOut();
        return this;
    }

    public boolean enable(String fromField, String toField) {
        return enable(fromField, toField, 30);// 默认以一个月30天区间分段查询
    }

    public boolean enable(String fromField, String toField, int offset) {
        DataRow headIn = dataIn.head();
        if (!headIn.getBoolean("segmentQuery"))
            return false;

        String cookieId = client.getCookieId();
        try (MemoryBuffer buff = new MemoryBuffer(SystemBuffer.Service.BigData, this.getClass().getName(), cookieId,
                MD5.get(dataIn.json()))) {
            if (buff.isNull()) {
                buff.setValue("beginDate", headIn.getDatetime(fromField));
                buff.setValue("endDate", headIn.getDatetime(toField).toDayEnd());
                buff.setValue("curBegin", headIn.getDatetime(fromField));
                buff.setValue("curEnd", headIn.getDatetime(fromField).toDayEnd());
                headIn.setValue(fromField, buff.getDatetime("beginDate"));
                headIn.setValue(toField, headIn.getDatetime(fromField).inc(DateType.Day, offset).toDayEnd());
                this.firstTime = true;
            } else {
                headIn.setValue(fromField, buff.getDatetime("curEnd").inc(DateType.Day, 1).toDayStart());
                headIn.setValue(toField, buff.getDatetime("curEnd").inc(DateType.Day, offset).toDayEnd());
                this.firstTime = false;
            }

            if (headIn.getDatetime(toField).compareTo(buff.getDatetime("endDate")) >= 0) {
                headIn.setValue(toField, buff.getDatetime("endDate"));
                buff.clear();
            } else {
                buff.setValue("curBegin", headIn.getDatetime(fromField));
                buff.setValue("curEnd", headIn.getDatetime(toField));
                buff.post();
                dataOut.head().setValue("_has_next_", true);
            }
        }
        return true;
    }

    public boolean isFirstTime() {
        return this.firstTime;
    }

    @Resource
    public void setAppClient(AppClient client) {
        this.client = client;
    }
}
