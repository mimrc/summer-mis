package cn.cerc.mis.core;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.ISession;

public interface IForm extends IHandle, IPermission, SupportBeanName {
    // 页面代码
    void setId(String formId);

    String getId();

    // 页面名称
    String getName();

    // 取得访问设备讯息
    AppClient getClient();

    // 设置参数
    void setParam(String key, String value);

    // 取得参数
    String getParam(String key, String def);

    default IPage execute(ISession session) throws Exception {
        this.setSession(session);
        return this.execute();
    }

    // 输出页面（支持jsp、reddirect、json等）
    IPage execute() throws Exception;

    // 执行指定函数，并返回jsp文件名，若自行处理输出则直接返回null
    String _call(String funcId) throws Exception;

    void setPathVariables(String[] pathVariables);

}
