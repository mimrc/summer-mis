package cn.cerc.mis.core;

import cn.cerc.db.exception.IKnowall;

public class RequestFailException extends RuntimeException implements IKnowall {

    private static final long serialVersionUID = 275988651753978198L;
    private final String[] data;

    public RequestFailException(Throwable cause, String... args) {
        super(cause);
        this.data = args;
    }

    @Override
    public String[] getData() {
        return data;
    }

}
