package cn.cerc.mis.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Utils;
import cn.cerc.db.core.Variant;

@Component
public class ServiceFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    /**
     * 返回指定的service对象，若为空时会抛出 ClassNotFoundException
     * 
     * @param handle      IHandle
     * @param serviceCode 服务代码
     * @param function    KeyValue
     * @return Service bean
     * @throws ClassNotFoundException 类文件异常
     */
    public Object get(IHandle handle, String serviceCode, Variant function) throws ClassNotFoundException {
        if (Utils.isEmpty(serviceCode))
            throw new ClassNotFoundException("serviceCode is null.");

        // 读取xml中的配置
        Object bean = null;
        if (applicationContext.containsBean(serviceCode)) {
            bean = applicationContext.getBean(serviceCode);
            if (bean instanceof IHandle temp)
                temp.setSession(handle.getSession());
            return bean;
        }

        // 读取注解的配置，并自动将第一个字母改为小写
        String[] params = serviceCode.split("\\.");
        // 支持指定执行函数
        if (params.length > 1)
            function.setValue(params[1]);
        String beanId = Application.getBeanIdOfClassCode(params[0]);
        if (applicationContext.containsBean(beanId)) {
            bean = applicationContext.getBean(beanId);
            if (bean instanceof IHandle temp)
                temp.setSession(handle.getSession());
            return bean;
        }
        // 先查找其实现
        if (applicationContext.containsBean(beanId + "Impl")) {
            bean = applicationContext.getBean(beanId + "Impl");
            if (bean instanceof IHandle temp)
                temp.setSession(handle.getSession());
            return bean;
        }
        // 提供虚拟服务
        if (applicationContext.getBeanNamesForType(ServiceSupplier.class).length > 0) {
            ServiceSupplier supplierService = applicationContext.getBean(ServiceSupplier.class);
            bean = supplierService.findService(handle, serviceCode);
            if (bean instanceof IHandle temp)
                temp.setSession(handle.getSession());
            if (bean != null)
                return bean;
        }

        throw new ClassNotFoundException(String.format("bean %s not find", serviceCode));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
