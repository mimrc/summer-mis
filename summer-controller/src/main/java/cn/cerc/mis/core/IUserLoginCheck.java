package cn.cerc.mis.core;

import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.StateMessage;

public interface IUserLoginCheck extends IHandle {

    // 登录验证
    StateMessage getToken(String userCode, String password, String device, String machineCode, String clientIP,
            String language, String loginType, String clientVersion);

    // 通过手机号获取帐号
    @Deprecated
    String getUserCode(String mobile) throws Exception;// FIXME 该方法不需要，所有关联的登录服务要调整

}
