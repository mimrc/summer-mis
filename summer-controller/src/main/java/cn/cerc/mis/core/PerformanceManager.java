package cn.cerc.mis.core;

import cn.cerc.db.core.DataSet;

public interface PerformanceManager {

    void writeExecuteTime(IForm form, String funcCode, long start, String dataIn);

    void writeServiceExecuteTime(String corpNo, String userCode, DataSet dataIn, String funcCode, long startTime);

    long getTotal();

}
