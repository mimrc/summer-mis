package cn.cerc.mis.core;

import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cerc.db.core.DataException;
import cn.cerc.db.core.DataRow;
import cn.cerc.db.core.DataSet;
import cn.cerc.db.core.IHandle;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.core.Variant;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.log.KnowallLog;
import cn.cerc.mis.client.ServiceExport;
import cn.cerc.mis.client.ServiceProxy;
import cn.cerc.mis.client.ServiceSign;
import cn.cerc.mis.security.SecurityStopException;

/**
 * 提供本地服务访问
 */
public class LocalService extends ServiceProxy {
    private static final Logger log = LoggerFactory.getLogger(LocalService.class);
    private static ServiceFactory serviceFactory;
    private String service;

    public LocalService(IHandle handle) {
        super();
        this.setSession(handle.getSession());
    }

    public LocalService(String service) {
        this.setService(service);
    }

    public LocalService(IHandle handle, String service) {
        super();
        this.setSession(handle.getSession());
        this.setService(service);
    }

    public LocalService(IHandle handle, ServiceSign service) {
        super();
        this.setSession(handle.getSession());
        this.setService(service.id());
    }

    public LocalService setService(String service) {
        this.service = service;
        return this;
    }

    public DataSet exec(IHandle handle, DataRow headIn) {
        return this.exec(handle, headIn.toDataSet());
    }

    public DataSet exec(IHandle handle, DataSet dataIn) {
        this.setSession(handle.getSession());
        this.setDataIn(dataIn);
        var dataOut = call(this.service(), handle, dataIn);
        this.setDataOut(dataOut);
        return dataOut;
    }

    // 带缓存调用服务
    public boolean exec(String... args) {
        if (args.length > 0) {
            DataRow headIn = dataIn().head();
            if (args.length % 2 != 0)
                throw new RuntimeException(Lang.get(LocalService.class, 1, "传入的参数数量必须为偶数！"));
            for (int i = 0; i < args.length; i = i + 2)
                headIn.setValue(args[i].toString(), args[i + 1]);
        }

        DataSet dataOut = LocalService.call(this.service, this, dataIn());
        this.setDataOut(dataOut);
        return this.isOk();
    }

    public final String service() {
        return service;
    }

    @Override
    public String message() {
        if (super.dataOut() != null && super.dataOut().message() != null) {
            return super.dataOut().message().replaceAll("'", "\"");
        } else {
            return null;
        }
    }

    @Deprecated
    public void setBufferRead(boolean value) {
        // 此属性已被移除
    }

    @Deprecated
    public String getService() {
        return service;
    }

    @Deprecated
    public DataSet getDataIn() {
        return dataIn();
    }

    @Deprecated
    public DataSet getDataOut() {
        return dataOut();
    }

    @Deprecated
    public String getMessage() {
        return message();
    }

    public static DataSet call(String key, IHandle handle, DataSet dataIn) {
        DataSet dataOut = new DataSet();
        IService service;
        Variant function = new Variant("execute").setKey(key);
        try {
            if (serviceFactory == null)
                serviceFactory = SpringBean.get(ServiceFactory.class);
            var bean = serviceFactory.get(handle, key, function);
            if (bean instanceof IService)
                service = (IService) bean;
            else
                return dataOut.setError().setMessage("not find service: " + key);
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(), e);
            return dataOut.setError().setMessage("not find service: " + key);
        }

        try {
            return service._call(handle, dataIn, function);
        } catch (Exception e) {
            Throwable throwable = e.getCause() != null ? e.getCause() : e;
            if (throwable instanceof IllegalArgumentException) {
                log.error("service {} 参数异常 {}", key, throwable.getMessage(), new KnowallException(throwable).add(key)
                        .add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));
            } else if (throwable instanceof InvocationTargetException)
                log.error("service {} 反射异常 {}", key, throwable.getMessage(), new KnowallException(throwable).add(key)
                        .add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));
            else if (throwable instanceof DataException) {
                // 追加堆栈
                KnowallLog klog = new KnowallLog();
                klog.setLevel("warn");
                klog.add(throwable);
                klog.add(handle.getCorpNo());
                klog.add(dataIn.json());
                klog.post();
            } else if (throwable instanceof SecurityStopException)
                log.warn("service {} 用户权限不足 {}", key, throwable.getMessage(), new KnowallException(throwable).add(key)
                        .add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));
            else if (throwable instanceof KnowallException knowall)
                log.error(throwable.getMessage(),
                        knowall.add(key).add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));
            else if (throwable instanceof RuntimeException)
                log.error("service {} 运行异常 {}", key, throwable.getMessage(), new KnowallException(throwable).add(key)
                        .add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));
            else
                log.error("service {} 其他异常 {}", key, throwable.getMessage(), new KnowallException(throwable).add(key)
                        .add(handle.getCorpNo()).addMapOf("dataIn", dataIn.json()));

            dataOut.setError().setMessage(throwable.getMessage());
            return dataOut;
        }
    }

    public String getExportKey() {
        return new ServiceExport(this, this.dataIn()).getExportKey();
    }

    @Deprecated
    public void setSign(ServiceSign sign) {
        this.service = sign.id();
    }

    @Deprecated
    public void setService(ServiceSign sign) {
        this.setSign(sign);
    }

    @Deprecated
    public String serviceId() {
        return this.service();
    }

    /**
     * 请改使用 SpringBean.get
     * 
     * @param <T>
     * @param class1
     * @return
     */
    @Deprecated
    public static <T> T target(Class<T> class1) {
        return SpringBean.get(class1);
    }
}
