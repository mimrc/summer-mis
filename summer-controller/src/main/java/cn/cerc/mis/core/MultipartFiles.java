package cn.cerc.mis.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import cn.cerc.db.core.ISession;
import jakarta.servlet.http.HttpServletRequest;

public class MultipartFiles {
    private List<DiskFileItem> items = new ArrayList<>();

    public static enum MultipartFileEnum {
        file1, file2, file3, file4, file5, file6, file7, file8, file9, audio,
    }

    public MultipartFiles(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        for (String name : params.keySet()) {
            DiskFileItem item = new DiskFileItem(name);
            String[] values = params.get(name);
            String value = "";
            for (int i = 0; i < values.length; i++) {
                value = (i == values.length - 1) ? value + values[i] : value + values[i] + ",";
            }
            item.setValue(value);
            items.add(item);
        }
    }

    public void addFiles(String fieldCode, MultipartFile[] files) {
        if (files != null) {
            for (var file : files)
                items.add(new DiskFileItem(fieldCode, file));
        }
    }

    public int size() {
        return items.size();
    }

    public DiskFileItem get(int i) {
        return items.get(i);
    }

    public List<DiskFileItem> items() {
        return items;
    }

    public static List<DiskFileItem> get(ISession session) {
        var obj = session.getProperty(ISession.FILES);
        if (obj instanceof MultipartFiles files)
            return files.items();
        return List.of();
    }

    public static List<DiskFileItem> get(IForm form) {
        return get(form.getSession());
    }
}
