package cn.cerc.mis.core;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

import jakarta.persistence.Column;

/**
 * 智能报表标识接口
 */
public interface IVuiReport {

    /**
     * 获取指定对象属性的代码和名字
     */
    static Map<String, String> buildFields(Class<?> clazz) {
        Map<String, String> items = new LinkedHashMap<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                items.put(field.getName(), column.name());
            }
        }
        return items;
    }

}
