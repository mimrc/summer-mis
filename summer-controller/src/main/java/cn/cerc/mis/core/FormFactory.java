package cn.cerc.mis.core;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.ISession;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.RequestSession;
import cn.cerc.db.core.Utils;
import cn.cerc.mis.other.PageNotFoundException;
import cn.cerc.mis.security.AutoLogin;
import cn.cerc.mis.security.Permission;
import cn.cerc.mis.security.SessionFactory;
import jakarta.annotation.PreDestroy;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FormFactory implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(FormFactory.class);
    private ApplicationContext context;
    private ISession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private static final AtomicBoolean errorOutput = new AtomicBoolean(false);

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
        Application.setContext(applicationContext);
    }

    public String getView(String formId, String funcCode, MultipartFiles files, String... pathVariables) {
        // 设置登录开关
        request.setAttribute("logon", false);
        if (files != null)
            session.setProperty(ISession.FILES, files);
        // 建立数据库资源
        try {
            IForm form = null;
            String beanId = formId;
            if (beanId == null || beanId.length() == 1) {
                // Frm不支持1个字符串长度的菜单
                throw new PageNotFoundException(request.getServletPath());
            }

            if (!Utils.isEmpty(beanId) && !"service".equals(beanId)) {
                if (!context.containsBean(beanId)) {
                    if (!beanId.substring(0, 2).toUpperCase().equals(beanId.substring(0, 2)))
                        beanId = beanId.substring(0, 1).toLowerCase() + beanId.substring(1);
                }
                if (context.containsBean(beanId)) {
                    try {
                        form = context.getBean(beanId, IForm.class);
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        throw new PageNotFoundException(request.getServletPath());
                    }
                } else {
                    Map<String, IForm> formMap = context.getBeansOfType(IForm.class).values().stream()
                            .collect(Collectors.toMap(item -> item.getClass().getSimpleName(), Function.identity()));
                    log.warn("危险的调用方式，请在2025年前移除：{}", formId);
                    form = formMap.get(formId);
                }
            }
            if (form == null && context.getBeanNamesForType(FormSupplier.class).length > 0) {
                try {
                    form = context.getBean(FormSupplier.class).getForm(session, formId, funcCode);
                } catch (NoSuchBeanDefinitionException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (form == null)
                throw new PageNotFoundException(request.getServletPath());

            if (form instanceof RedirectForm owner) {
                var redirectForm = owner.getRedirectForm(form);
                if (redirectForm != null)
                    form = redirectForm;
            }

            form.setSession(session);
            // 取得页面cookie传递进来的sid，并将sid进行保存
            String token = form.getClient().getToken();
            if (!Utils.isEmpty(token))
                SessionFactory.loadToken(session, token);

            // 取出自定义session中用户设置的语言类型，并写入到request
            request.setAttribute(ISession.LANGUAGE_ID, Lang.id());
            request.setAttribute("_showMenu_", !AppClient.ee.equals(form.getClient().getDevice()));// 如果页面带device，则同时更新

            form.setId(formId);
            // 传递路径变量
            form.setPathVariables(pathVariables);

            // 匿名访问
            Permission ps = form.getClass().getAnnotation(Permission.class);
            if (ps != null && Permission.GUEST.equals(ps.value()))
                return form._call(funcCode);

            // 是否登录
            if (context.getBeanNamesForType(AutoLogin.class).length > 0) {
                context.getBean(AutoLogin.class).execute(session);
            } else if (!errorOutput.get()) {
                log.error("未配置 AutoLogin 对象");
                errorOutput.set(true);
            }
            if (!session.logon()) // 显示登录对话框
                return Application.getBean(form, IAppLogin.class).getLoginView(form);

            // 设备检查
            if (form.isSecurityDevice())
                return form._call(funcCode);

            ISecurityDeviceCheck deviceCheck = Application.getBean(form, ISecurityDeviceCheck.class);
            switch (deviceCheck.pass(form)) {
            case permit:
                log.debug("{}.{}", formId, funcCode);
                SessionFactory.switchCustomDatabase(session);
                return form._call(funcCode);
            case check: // 显示设备验证码对话框
                var frmVerifyClient = context.getBean("frmVerifyClient", IForm.class);
                frmVerifyClient.setSession(session);
                return form.execute().execute();
            case login: // 再一次显示登录对话框
                return Application.getBean(form, IAppLogin.class).getLoginView(form);
            default:
                outputErrorPage(new RuntimeException(Lang.get(FormFactory.class, 1, "对不起，当前设备被禁止使用！")));
                return null;
            }
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause() != null ? e.getCause() : e;
            outputErrorPage(cause);
            return null;
        } catch (Exception e) {
            outputErrorPage(e);
            return null;
        }
    }

    private void outputErrorPage(Throwable e) {
        response.setContentType("text/html;charset=UTF-8");
        if (context.getBeanNamesForType(IErrorPage.class).length > 0) {
            IErrorPage error = context.getBean(IErrorPage.class);
            error.output(e);
        } else {
            try {
                response.getOutputStream().print("error: " + e.getMessage());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Resource
    public void setSession(RequestSession session) {
        this.session = session;
    }

    @PreDestroy
    public void close() {
        session.close();
    }

    @Resource
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Resource
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

}
