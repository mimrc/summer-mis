package cn.cerc.mis.core;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import cn.cerc.db.core.SpringBean;
import cn.cerc.mis.core.MultipartFiles.MultipartFileEnum;

public class DiskFileItem {
    private static final Logger log = LoggerFactory.getLogger(DiskFileItem.class);
    private MultipartFile source;
    private boolean isFormField;
    private String value;
    private String fieldName;

    public DiskFileItem(String fieldName, MultipartFile source) {
        this.source = source;
        this.fieldName = fieldName;
        this.isFormField = false;
    }

    public DiskFileItem(String fieldName) {
        this.fieldName = fieldName;
        this.isFormField = true;
    }

    public DiskFileItem() {
        this.isFormField = true;
    }

    public boolean isFormField() {
        return isFormField;
    }

    public long getSize() {
        return source.getSize();
    }

    /**
     * 获取文件原始名称
     */
    public String getName() {
        return source.getOriginalFilename();
    }

    public byte[] get() throws IOException {
        return source.getBytes();
    }

    public InputStream getInputStream() {
        try {
            return source.getInputStream();
        } catch (IOException e) {
            System.err.println(this.getClass().getName() + ":" + e.getMessage());
            SpringBean.printStackTrace(this.getClass());
            return null;
        }
    }

    /**
     * 获取提交字段的名称
     */
    public String getFieldName() {
        if (this.isFormField)
            return this.fieldName;
        else
            return source.getName();
    }

    public MultipartFileEnum getFileId() {
        if (this.isFormField)
            return null;
        try {
            return MultipartFileEnum.valueOf(this.fieldName);
        } catch (IllegalArgumentException e) {
            log.error("不支持的上传文件id: {}", this.fieldName);
            return null;
        }
    }

    /**
     * 获取字段的值
     */
    public String getString() {
        if (this.isFormField)
            return this.value;
        else
            return source.getOriginalFilename();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Object getContentType() {
        return source.getContentType();
    }

    /**
     * 自定义文件名
     * @param fileName 文件名
     */
    public void setFieldName(String fileName) {
        this.fieldName = fileName;
        this.isFormField = true;
    }
}
