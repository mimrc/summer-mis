package cn.cerc.mis.core;

import cn.cerc.db.core.HtmlWriter;

public interface HtmlContent {

    void output(HtmlWriter html);

}
