package cn.cerc.mis.tools;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import cn.cerc.db.core.DataSetException;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.SpringBean;
import cn.cerc.db.log.KnowallException;
import cn.cerc.db.tool.JsonTool;
import cn.cerc.mis.client.ServiceExecuteException;
import cn.cerc.mis.core.AbstractForm;
import cn.cerc.mis.core.IForm;
import cn.cerc.mis.core.IPage;
import cn.cerc.mis.core.PerformanceManager;
import cn.cerc.mis.security.SecurityPolice;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class FormUtils {
    private static final Logger log = LoggerFactory.getLogger(FormUtils.class);

    @Autowired
    private SecurityPolice securityPolice;

    // 执行指定函数，并返回jsp文件名，若自行处理输出则直接返回null
    public IPage get(IForm form, String funcCode, String[] pathVariables)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, ServletException, IOException, ServiceExecuteException {
        long start = System.currentTimeMillis();
        try {
            HttpServletResponse response = form.getResponse();
            if ("excel".equals(funcCode)) {
                response.setContentType("application/vnd.ms-excel; charset=UTF-8");
                response.addHeader("Content-Disposition", "attachment; filename=excel.csv");
            } else {
                response.setContentType("text/html;charset=UTF-8");
            }

            Object result;
            Method method;
            try {
                if (pathVariables.length > 0)
                    log.error("2025年起，不再支持此种写法！");
                // 支持路径参数调用，最多3个字符串参数
                switch (pathVariables.length) {
                case 1: {
                    if (form.getClient().isPhone()) {
                        try {
                            method = form.getClass().getMethod(funcCode + "_phone", String.class);
                        } catch (NoSuchMethodException e) {
                            method = form.getClass().getMethod(funcCode, String.class);
                        }
                    } else {
                        method = form.getClass().getMethod(funcCode, String.class);
                    }
                    securityPolice.check(form, method, form);
                    result = method.invoke(form, pathVariables[0]);
                    break;
                }
                case 2: {
                    if (form.getClient().isPhone()) {
                        try {
                            method = form.getClass().getMethod(funcCode + "_phone", String.class, String.class);
                        } catch (NoSuchMethodException e) {
                            method = form.getClass().getMethod(funcCode, String.class, String.class);
                        }
                    } else {
                        method = form.getClass().getMethod(funcCode, String.class, String.class);
                    }
                    securityPolice.check(form, method, form);
                    result = method.invoke(form, pathVariables[0], pathVariables[1]);
                    break;
                }
                case 3: {
                    if (form.getClient().isPhone()) {
                        try {
                            method = form.getClass().getMethod(funcCode + "_phone", String.class, String.class,
                                    String.class);
                        } catch (NoSuchMethodException e) {
                            method = form.getClass().getMethod(funcCode, String.class, String.class, String.class);
                        }
                    } else {
                        method = form.getClass().getMethod(funcCode, String.class, String.class, String.class);
                    }
                    securityPolice.check(form, method, form);
                    result = method.invoke(form, pathVariables[0], pathVariables[1], pathVariables[2]);
                    break;
                }
                default: {
                    if (form.getClient().isPhone()) {
                        method = findMethod(form.getClass(), funcCode + "_phone");
                        if (method == null)
                            method = findMethod(form.getClass(), funcCode);
                    } else {
                        method = findMethod(form.getClass(), funcCode);
                    }
                    if (method == null)
                        throw new NoSuchMethodException(
                                String.format(Lang.get(AbstractForm.class, 1, "找不到目标可执行函数 %s"), funcCode));
                    securityPolice.check(form, method, form);
                    if (method.getParameterCount() > 0) {
                        log.warn("2025年起，不再支持此种写法！");
                        Object[] args = new Object[method.getParameterCount()];
                        List<String> list = new ArrayList<>();
                        Enumeration<String> parameterNames = form.getRequest().getParameterNames();
                        while (parameterNames.hasMoreElements())
                            list.add(parameterNames.nextElement());

                        if (list.size() < method.getParameters().length)
                            throw new IllegalArgumentException(Lang.get(AbstractForm.class, 2, "参数传入个数小于方法声明需要的参数数量"));

                        int i = 0;
                        for (Parameter arg : method.getParameters()) {
                            String tmp = form.getRequest().getParameter(list.get(i));
                            PathVariable pathVariable = arg.getAnnotation(PathVariable.class);
                            if (pathVariable != null)
                                tmp = form.getRequest().getParameter(pathVariable.value());

                            String paramType = arg.getParameterizedType().getTypeName();
                            if ("int".equals(paramType) || Integer.class.getName().equals(paramType))
                                args[i++] = Integer.parseInt(tmp);
                            else if (String.class.getName().equals(paramType))
                                args[i++] = tmp;
                            else
                                throw new UnsupportedOperationException(
                                        String.format(Lang.get(AbstractForm.class, 3, "不支持的参数类型 %s"), paramType));
                        }
                        result = method.invoke(form, args);
                    } else {
                        result = method.invoke(form);
                    }
                }
                }

                if (result == null)
                    return new StringPage(form, null);

                if (result instanceof IPage output) {
                    return output;
                } else {
                    var e = new KnowallException()
                            .add(String.format(Lang.get(AbstractForm.class, 4, "页面 %s.%s 返回值为 %s，它应该改为实现 IPage 接口的对象"),
                                    form.getClass().getSimpleName(), funcCode, result));
                    log.warn(e.getMessage(), e);
                    return new StringPage(form, (String) result);
                }
            } catch (Exception e) {
                if (e.getCause() instanceof DataSetException dse) {
                    if (dse.getObject() instanceof IPage page1) {
                        page1.setMessage(dse.getMessage());
                        return page1;
                    }
                }
                throw e;
            }
        } finally {
            var context = SpringBean.context();
            if (context.getBeanNamesForType(PerformanceManager.class).length == 1) {
                var pm = context.getBean(PerformanceManager.class);
                // 使用 CompletableFuture 将代码放到异步线程中运行
                String dataIn = Optional.ofNullable(form.getRequest()).map(ServletRequest::getParameterMap)
                        .map(JsonTool::toJson).orElse("");
                pm.writeExecuteTime(form, funcCode, start, dataIn);
            }
        }
    }

    private Method findMethod(Class<?> clazz, String funcCode) {
        for (Method item : clazz.getMethods()) {
            if (funcCode.equals(item.getName()))
                return item;
        }
        return null;
    }

}
